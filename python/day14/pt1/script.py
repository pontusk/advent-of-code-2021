from collections import Counter

def main(data):
    template, rules = data.split("\n\n")
    rules = [tuple(rule.split(" -> ")) for rule in rules.splitlines()]

    str = template

    for _ in range(0, 10):
        new_str = ""
        for i, char1 in enumerate(str):
            char2 = str[i + 1 : i + 2]
            pair = f"{char1}{char2}"
            rule = ("", "")

            temp = [p[0] for p in rules]
            if pair in temp:
                rule = rules[temp.index(pair)]

            new_str = f"{new_str}{char1}{rule[1]}"
        str = new_str

    c = Counter(str)

    return max(c.values()) - min(c.values())
