import unittest, io, os, contextlib
from . import script

with io.open(os.path.join(os.path.dirname(__file__), "..", "test_input"), "r", encoding="utf8") as f:
    test_data = f.read()

with io.open(os.path.join(os.path.dirname(__file__), "..", "input"), "r", encoding="utf8") as f:
    data = f.read()


class TestScript(unittest.TestCase):
    def test_main(self):

        test_result = script.main(test_data)
        print(f"\nTest result: {test_result}\n")
        self.assertEqual(test_result, 2188189693529)

        with open(os.devnull, 'w') as devnull:
            with contextlib.redirect_stdout(devnull):
                result = script.main(data)

        print(f"Final result: {result}\n")


if __name__ == "__main__":
    unittest.main()

# Run test with ex. python -m python.day15.pt2.test from root dir
