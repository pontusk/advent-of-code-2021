from collections import Counter

def main(data):
    template, rules = data.split("\n\n")
    rules = [tuple(rule.split(" -> ")) for rule in rules.splitlines()]

    pairs = Counter()
    tally = Counter()

    for i, char1 in enumerate(template):
        char2 = template[i + 1 : i + 2]
        tally[char1] += 1

        if char2:
            pairs[f"{char1}{char2}"] += 1

    for _ in range(0, 40):
        new_pairs = Counter()
        for pair, count in pairs.items():
            rule = ("", "")

            temp = [p[0] for p in rules]
            if pair in temp:
                rule = rules[temp.index(pair)]

            tally[rule[1]] += count

            new_pairs[f"{pair[0]}{rule[1]}"] += count
            new_pairs[f"{rule[1]}{pair[1]}"] += count

        pairs = new_pairs

    return max(tally.values()) - min(tally.values())
