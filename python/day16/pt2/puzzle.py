from typing import TypedDict
from typing import Optional
import math

Packet = TypedDict("Packet", {
    "version": Optional[int],
    "type_id": Optional[int],
    "value": Optional[int],
    "sub_packets": list["Packet"],
    "cursor": int,
})


def to_bin(hex: str) -> str:
    key = {
        "0": "0000",
        "1": "0001",
        "2": "0010",
        "3": "0011",
        "4": "0100",
        "5": "0101",
        "6": "0110",
        "7": "0111",
        "8": "1000",
        "9": "1001",
        "A": "1010",
        "B": "1011",
        "C": "1100",
        "D": "1101",
        "E": "1110",
        "F": "1111",
    }
    binary = ""
    for char in hex.strip():
        binary += key[char]
    return binary


def parse(data: str) -> str:
    return to_bin(data)


def decode(packet: str) -> Packet:
    cursor: int = 0
    bits: str = packet[cursor:cursor + 3]
    version: Optional[int] = None
    type_id: Optional[int] = None
    sub_packets: list[Packet] = []
    literal: str = ""
    value: Optional[int] = None
    length_type: Optional[int] = None

    def pick(n: int) -> None:
        nonlocal cursor
        nonlocal bits
        bits = packet[cursor: cursor + n]
        cursor += n

    pick(3)
    version = int(bits, 2)

    pick(3)
    type_id = int(bits, 2)

    if type_id == 4:
        while True:
            pick(5)
            prefix = bits[:1]
            rest = bits[1:]
            literal += rest
            if prefix == "0":
                break
        value = int(literal, 2)
    else:
        pick(1)
        length_type = int(bits)

        if length_type == 0:
            pick(15)
            sub_packets_length = int(bits, 2)
            pick(sub_packets_length)
            sub_packets_cursor = 0
            while sub_packets_cursor < sub_packets_length:
                sub_packet = decode(bits[sub_packets_cursor:])
                sub_packets.append(sub_packet)
                sub_packets_cursor += sub_packet["cursor"]

        elif length_type == 1:
            pick(11)
            sub_packets_number = int(bits, 2)
            while len(sub_packets) < sub_packets_number:
                sub_packet = decode(packet[cursor:])
                sub_packets.append(sub_packet)
                pick(sub_packet["cursor"])

        else:
            raise Exception(
                f"length_type can only be 1 or 0, but it was {length_type}")

        if type_id == 0:  # Sum
            value = sum(sub_packet["value"] or 0 for sub_packet in sub_packets)

        elif type_id == 1:  # Product
            value = math.prod(
                sub_packet["value"] or 0 for sub_packet in sub_packets)

        elif type_id == 2:  # Min
            value = min(
                sub_packet["value"] or 0 for sub_packet in sub_packets)

        elif type_id == 3:  # Max
            value = max(
                sub_packet["value"] or 0 for sub_packet in sub_packets)

        elif type_id in range(5, 8):
            p1 = 0
            p2 = 0
            if len(sub_packets) == 2:
                p1 = sub_packets[0]["value"] or 0
                p2 = sub_packets[1]["value"] or 0
            else:
                raise Exception(
                    f"sub_packets should be 2, but were {len(sub_packets)}")

            if type_id == 5:  # Greater
                value = 1 if p1 > p2 else 0

            if type_id == 6:  # Less
                value = 1 if p1 < p2 else 0

            if type_id == 7:  # Equal
                value = 1 if p1 == p2 else 0

        else:
            raise Exception(
                f"type_id can only be in the range 0 to 7, but it was {type_id}")

    return {
        "version": version,
        "type_id": type_id,
        "value": value,
        "sub_packets": sub_packets,
        "cursor": cursor,
    }


def solve(data: str) -> Packet:
    packet = parse(data)
    return decode(packet)
