import unittest
import io
import os
import contextlib
import puzzle
from puzzle import Packet

with io.open(
    os.path.join(os.path.dirname(__file__), "..", "input"),
    "r",
    encoding="utf8"
) as f:
    data = f.read()


class TestScript(unittest.TestCase):
    def test_pass(self) -> None:
        literal_answer = puzzle.solve("D2FE28")
        self.assertEqual(2021, literal_answer["value"])

        len_type_zero_answer = puzzle.solve("38006F45291200")
        self.assertEqual(10, len_type_zero_answer["sub_packets"][0]["value"])
        self.assertEqual(20, len_type_zero_answer["sub_packets"][1]["value"])

        len_type_one_answer = puzzle.solve("EE00D40C823060")
        self.assertEqual(1, len_type_one_answer["sub_packets"][0]["value"])
        self.assertEqual(2, len_type_one_answer["sub_packets"][1]["value"])
        self.assertEqual(3, len_type_one_answer["sub_packets"][2]["value"])

        def count(packet: Packet) -> int:
            sum: int | None = packet["version"]
            if len(packet["sub_packets"]) > 0:
                for sub_packet in packet["sub_packets"]:
                    if sum is not None:
                        sum += count(sub_packet)
            return sum or 0

        sum_1_answer = puzzle.solve("8A004A801A8002F478")
        self.assertEqual(16, count(sum_1_answer))
        sum_2_answer = puzzle.solve("620080001611562C8802118E34")
        self.assertEqual(12, count(sum_2_answer))
        sum_3_answer = puzzle.solve("C0015000016115A2E0802F182340")
        self.assertEqual(23, count(sum_3_answer))
        sum_4_answer = puzzle.solve("A0016C880162017C3686B18A3D4780")
        self.assertEqual(31, count(sum_4_answer))

        answer_1 = puzzle.solve("C200B40A82")
        self.assertEqual(3, answer_1["value"])
        answer_2 = puzzle.solve("04005AC33890")
        self.assertEqual(54, answer_2["value"])
        answer_3 = puzzle.solve("880086C3E88112")
        self.assertEqual(7, answer_3["value"])
        answer_4 = puzzle.solve("CE00C43D881120")
        self.assertEqual(9, answer_4["value"])
        answer_5 = puzzle.solve("D8005AC2A8F0")
        self.assertEqual(1, answer_5["value"])
        answer_6 = puzzle.solve("F600BC2D8F")
        self.assertEqual(0, answer_6["value"])
        answer_7 = puzzle.solve("9C005AC2F8F0")
        self.assertEqual(0, answer_7["value"])
        answer_8 = puzzle.solve("9C0141080250320F1802104A08")
        self.assertEqual(1, answer_8["value"])

        with open(os.devnull, "w") as devnull:
            with contextlib.redirect_stdout(devnull):
                answer = puzzle.solve(data)

        sum = count(answer)

        self.assertEqual(955, sum)

        print(f"Final answer: {answer['value']}\n")


if __name__ == "__main__":
    unittest.main()
