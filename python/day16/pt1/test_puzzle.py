import unittest
import io
import os
import contextlib
import puzzle
from puzzle import Packet

with io.open(
    os.path.join(os.path.dirname(__file__), "..", "input"),
    "r",
    encoding="utf8"
) as f:
    data = f.read()


class TestScript(unittest.TestCase):
    def test_pass(self) -> None:
        literal_answer = puzzle.solve("D2FE28")
        self.assertEqual(2021, literal_answer["value"])

        len_type_zero_answer = puzzle.solve("38006F45291200")
        self.assertEqual(10, len_type_zero_answer["sub_packets"][0]["value"])
        self.assertEqual(20, len_type_zero_answer["sub_packets"][1]["value"])

        len_type_one_answer = puzzle.solve("EE00D40C823060")
        self.assertEqual(1, len_type_one_answer["sub_packets"][0]["value"])
        self.assertEqual(2, len_type_one_answer["sub_packets"][1]["value"])
        self.assertEqual(3, len_type_one_answer["sub_packets"][2]["value"])

        def count(packet: Packet) -> int:
            sum: int | None = packet["version"]
            if len(packet["sub_packets"]) > 0:
                for sub_packet in packet["sub_packets"]:
                    if sum is not None:
                        sum += count(sub_packet)
            return sum or 0

        sum_1_answer = puzzle.solve("8A004A801A8002F478")
        self.assertEqual(16, count(sum_1_answer))
        sum_2_answer = puzzle.solve("620080001611562C8802118E34")
        self.assertEqual(12, count(sum_2_answer))
        sum_3_answer = puzzle.solve("C0015000016115A2E0802F182340")
        self.assertEqual(23, count(sum_3_answer))
        sum_4_answer = puzzle.solve("A0016C880162017C3686B18A3D4780")
        self.assertEqual(31, count(sum_4_answer))

        with open(os.devnull, "w") as devnull:
            with contextlib.redirect_stdout(devnull):
                answer = puzzle.solve(data)

        sum = count(answer)

        print(f"Final answer: {sum}\n")


if __name__ == "__main__":
    unittest.main()
