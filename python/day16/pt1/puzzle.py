from typing import TypedDict
from typing import Optional

Packet = TypedDict("Packet", {
    "version": Optional[int],
    "type_id": Optional[int],
    "value": Optional[int],
    "sub_packets": list["Packet"],
    "cursor": int,
})


def to_bin(hex: str) -> str:
    key = {
        "0": "0000",
        "1": "0001",
        "2": "0010",
        "3": "0011",
        "4": "0100",
        "5": "0101",
        "6": "0110",
        "7": "0111",
        "8": "1000",
        "9": "1001",
        "A": "1010",
        "B": "1011",
        "C": "1100",
        "D": "1101",
        "E": "1110",
        "F": "1111",
    }
    binary = ""
    for char in hex.strip():
        binary += key[char]
    return binary


def parse(data: str) -> str:
    return to_bin(data)


def decode(packet: str) -> Packet:
    cursor = 0
    bits = packet[cursor:cursor + 3]
    version = None
    type_id = None
    sub_packets: list[Packet] = []
    literal = ""
    value = None
    length_type = None

    def pick(n: int) -> None:
        nonlocal cursor
        nonlocal bits
        bits = packet[cursor: cursor + n]
        cursor += n

    while True:
        if packet[cursor:] == str(len(packet[cursor:]) * 0):
            break

        if version is None:
            pick(3)
            version = int(bits, 2)
            continue

        if type_id is None:
            pick(3)
            type_id = int(bits, 2)

        if type_id == 4:  # Literal value
            pick(5)
            prefix = bits[:1]
            rest = bits[1:]
            literal += rest

            if prefix == "0":
                break

            continue

        if length_type is None:
            pick(1)
            length_type = bits

        if length_type == "0":
            pick(15)
            sub_packets_length = int(bits, 2)
            pick(sub_packets_length)
            sub_packets_cursor = 0
            while sub_packets_cursor < sub_packets_length:
                sub_packet = decode(bits[sub_packets_cursor:])
                sub_packets.append(sub_packet)
                sub_packets_cursor += sub_packet["cursor"]
            break

        if length_type == "1":
            pick(11)
            sub_packets_number = int(bits, 2)
            while len(sub_packets) < sub_packets_number:
                sub_packet = decode(packet[cursor:])
                sub_packets.append(sub_packet)
                pick(sub_packet["cursor"])
            break

    if literal != "":
        value = int(literal, 2)

    return {
        "version": version,
        "type_id": type_id,
        "value": value,
        "sub_packets": sub_packets,
        "cursor": cursor,
    }


def solve(data: str) -> Packet:
    packet = parse(data)
    return decode(packet)
