def plot_dict(map):
    max_y = max(y for _, y in map.keys())
    max_x = max(x for x, _ in map.keys())

    print("")
    for y in range(0, max_y + 1):
        for x in range(0, max_x + 1):
            if (x, y) in map:
                print(map[(x, y)], end="")
            else:
                print(".", end="")
        print("")
    print("")


def plot_dots(dots):
    max_y = max(y for _, y in dots)
    max_x = max(x for x, _ in dots)

    print("")
    for y in range(0, max_y + 1):
        for x in range(0, max_x + 1):
            if (x, y) in dots:
                print("#", end="")
            else:
                print(".", end="")
        print("")
    print("")


def find_surrounding(map, point):
    points = map.keys()
    x, y = point
 
    surrounding = []

    top     = (x, y - 1)
    right   = (x + 1, y)
    bottom  = (x, y + 1)
    left    = (x - 1, y)

    if (top in points):
        surrounding.append(top)
    if (right in points):
        surrounding.append(right)
    if (bottom in points):
        surrounding.append(bottom)
    if (left in points):
        surrounding.append(left)

    return surrounding
