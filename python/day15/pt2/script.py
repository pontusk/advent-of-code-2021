import heapq
from ... import utils

def grow_map(map, axis):
    max_point = max(map)
    new_map = map.copy()

    for point in map:
        x, y = point

        for i in range(4):
            increment = (max_point[axis] + 1) * (i + 1) 

            incremented_point = [(x + increment, y), (x, y + increment)]

            new_level = map[point] + i + 1

            if new_level > 9:
                new_level -= 9

            new_map[incremented_point[axis]] = new_level

    return new_map


def main(data):
    map = {}
    for y, line in enumerate(data.splitlines()):
        for x, level in enumerate(line):
            map[(x,y)] = int(level)

    map = grow_map(map, 0)
    map = grow_map(map, 1)

    best_level_at = {}
    todo = [(0, (0, 0))]
    end = max(map)


    while (todo):
        level, point = heapq.heappop(todo)
        surrounding = utils.find_surrounding(map, point)

        if point in best_level_at and level >= best_level_at[point]:
            continue
        else:
            best_level_at[point] = level

        if point == end:
            break

        for p in surrounding:
            heapq.heappush(todo, (level + map[p], p))

    utils.plot_dict(map)

    return best_level_at[end]
