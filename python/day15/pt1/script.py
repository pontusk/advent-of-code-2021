import heapq
from ... import utils

def main(data):
    map = {}
    for y, line in enumerate(data.splitlines()):
        for x, level in enumerate(line):
            map[(x,y)] = int(level)

    best_level_at = {}
    todo = [(0, (0, 0))]
    end = max(map)

    while (todo):
        level, point = heapq.heappop(todo)
        surrounding = utils.find_surrounding(map, point)

        if point in best_level_at and level >= best_level_at[point]:
            continue
        elif point in map:
            best_level_at[point] = level

        if point == end:
            break

        for p in surrounding:
            if p in map:
                heapq.heappush(todo, (level + map[p], p))


    return best_level_at[end]
