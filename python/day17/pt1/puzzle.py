import re

Velocity = tuple[int, int]
Vector = tuple[int, int]
Area = list[Vector]
Trajectory = list[Vector]


def parse(data: str) -> Area:
    [(x_min, x_max), (y_min, y_max)] = re.findall(
        "\\=([-\\d]+)\\.\\.([-\\d]+)", data)

    return [(int(x_min), int(x_max)), (int(y_min), int(y_max))]


def step(args: tuple[Velocity, Trajectory]) -> tuple[Velocity, Trajectory]:
    ((x_velocity, y_velocity), trajectory) = args

    (x, y) = trajectory[-1]

    x += x_velocity
    y += y_velocity

    if x_velocity < 0:
        x_velocity += 1

    if x_velocity > 0:
        x_velocity -= 1

    y_velocity -= 1

    trajectory.append((x, y))

    return ((x_velocity, y_velocity), trajectory)


def in_target_area(vector: Vector, area: Area) -> bool:
    (x, y) = vector
    x_max_area = area[0][1]
    y_max_area = area[1][1]
    x_min_area = area[0][0]
    y_min_area = area[1][0]
    return (x >= x_min_area and
            x <= x_max_area and
            y >= y_min_area and
            y <= y_max_area)


def shot_status(vector: Vector, area: Area) -> str:
    (x, y) = vector
    x_max_area = area[0][1]
    x_min_area = area[0][0]
    y_min_area = area[1][0]
    if in_target_area(vector, area):
        return "hit"
    if (x > x_max_area or
            y < y_min_area):
        if x > x_max_area:
            return "long_miss"
        elif x < x_min_area:
            return "short_miss"
        else:
            return "miss"
    else:
        return "pend"


def plot(trajectory: Trajectory, area: Area) -> None:
    x_max_area = area[0][1]
    y_max_area = area[1][1]
    x_min_area = area[0][0]
    y_min_area = area[1][0]
    vectors = trajectory + [
        (x_max_area, y_max_area),
        (x_min_area, y_min_area)]
    y_min = min(i[1] for i in vectors)
    y_max = max(i[1] for i in vectors)
    x_max = max(i[0] for i in vectors)
    for y in reversed(range(y_min, y_max + 1)):
        for x in range(0, x_max + 1):
            if (x, y) == trajectory[0]:
                print("S", end="")
            elif (x, y) in trajectory:
                print("#", end="")
            elif in_target_area((x, y), area):
                print("T", end="")
            else:
                print(".", end="")
        print("")


def first_test(velocity: Velocity, area: Area) -> None:
    s = (velocity, [(0, 0)])

    test = step(s)

    if test[1][-1][0] < area[0][1]:
        raise Exception("Shot too short. Try a higher initial velocity.")


def test_velocity(velocity: Velocity, area: Area) -> tuple[int, str]:
    s = (velocity, [(0, 0)])

    while True:
        s = step(s)
        shot = shot_status(s[1][-1], area)
        if shot != "pend":
            break

    max_y = max(i[1] for i in s[1])
    plot(s[1], area)
    print(f"Shot status: {shot}")

    return (max_y, shot)


def solve(data: str) -> int:
    area = parse(data)

    initial_velocity = (area[0][1] + 1, abs(area[1][0]))
    velocity = initial_velocity
    min_x = 0
    max_x = initial_velocity[0]
    answer = 0
    shot = "pend"

    first_test(velocity, area)

    while shot != "hit":
        (answer, shot) = test_velocity(velocity, area)
        (x, y) = velocity

        if shot == "short_miss":
            delta = max(max_x - x, 1)
            min_x = x
            velocity = (int(x + delta / 2), y)
        elif shot == "long_miss":
            delta = max(x - min_x, 1)
            max_x = x
            velocity = (int(min_x + delta / 2), y)
        else:
            velocity = (initial_velocity[0], y - 1)

    return answer
