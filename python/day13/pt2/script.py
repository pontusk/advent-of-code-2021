from ... import utils


dots = set()

def main(data):
    instr = []
    dots.clear()

    top, bottom = data.split("\n\n")

    for line in top.splitlines():
        x, y = line.split(",")
        dots.add((int(x), int(y)))

    for line in bottom.splitlines():
        dir, num = line.split("=")
        instr.append([dir[-1], int(num)])
        
    def fold(ins):
        dir, num = ins
        global dots

        if dir == "x":
            dots = {(x if x < num else num * 2 - x, y) for x, y in dots}

        if dir == "y":
            dots = {(x, y if y < num else num * 2 - y) for x, y in dots}

    for ins in instr:
        fold(ins)

    utils.plot_dots(dots)

    return 1
