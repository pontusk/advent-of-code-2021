def main(data):
    top, bottom = data.split("\n\n")

    paper = []
    instr = []

    for line in top.splitlines():
        paper.append([int(num) for num in line.split(",")])

    for line in bottom.splitlines():
        dir, num = line.split("=")
        instr.append([dir[-1], int(num)])

    dots = set()


    def fold(ins):
        dir, num = ins

        if dir == "x":
            for i in range(len(paper)):
                x = paper[i][0]
                y = paper[i][1]

                if paper[i][0] > num:
                    x = num * 2 - x

                dots.add((x, y))

        if dir == "y":
            for i in range(len(paper)):
                x = paper[i][0]
                y = paper[i][1]

                if paper[i][1] > num:
                    y = num * 2 - y

                dots.add((x, y))


    fold(instr[0])

    return len(dots)
