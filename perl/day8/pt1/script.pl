#!/usr/bin/env perl
use v5.14;
use Data::Dumper;

open my $fh, "<", "../input" or die "Couldn't open 'input': $!";

my @reference = (
    'abcefg', 'cf',  'acdeg',   'acdfg', 'bcdf', 'abdfg',
    'abdefg', 'acf', 'abcdefg', 'abcdfg',
);

my @found = ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, );

for (<$fh>) {
    my ( $pattern, $output ) = split /\|/, $_;

    my @output = grep /\w+/, split / /, $output =~ s/\n//r;

    for (@output) {
        my $length = length($_);

        $found[1]++ if $length == length( $reference[1] );
        $found[4]++ if $length == length( $reference[4] );
        $found[7]++ if $length == length( $reference[7] );
        $found[8]++ if $length == length( $reference[8] );
    }
}

my $sum = 0;

for (@found) { $sum += $_; }

say $sum;

close $fh;
