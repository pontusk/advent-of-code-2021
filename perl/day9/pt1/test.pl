#!/usr/bin/env perl
use v5.14;
use Data::Dumper;

open my $fh, "<", "../test_input" or die "Couldn't open 'input': $!";

my @data = <$fh>;
my @low_points;

for (@data) {
    state $index = 0;
    my @line = grep /\d/, split //, $_;
    $data[$index] = \@line;
    $index++;
}

for (@data) {
    state $l = 0;
    my $c    = 0;
    my @line = @{$_};

    for (@line) {
        my $val = $data[$l]->[$c];
        my @sur;

        # top
        @sur[ ++$#sur ] = $data[ $l - 1 ]->[$c] if $l - 1 >= 0;

        # right
        @sur[ ++$#sur ] = $data[$l]->[ $c + 1 ] if $c + 1 < scalar(@line);

        # bottom
        @sur[ ++$#sur ] = $data[ $l + 1 ]->[$c] if $l + 1 < scalar(@data);

        # left
        @sur[ ++$#sur ] = $data[$l]->[ $c - 1 ] if $c - 1 >= 0;

        my $lowest = $sur[0];
        for (@sur) { $lowest = $_ if $_ < $lowest; }

        @low_points[ ++$#low_points ] = $val if $val < $lowest;

        $c++;
    }

    $c = 0;
    $l++;
}

my $risk_level = 0;

for (@low_points) {
    $risk_level += $_ + 1;
}

say $risk_level;
say Dumper @low_points;

close $fh;
