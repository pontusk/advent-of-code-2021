#!/usr/bin/env perl
use v5.14;
use Data::Dumper;

open my $fh, "<", "../input" or die "Couldn't open 'input': $!";

my @original = grep /\d/, <$fh>;

sub findRating {
    my @all = @original;
    my @del;
    my $dist;
    my $cur_i = 0;

    while ( scalar(@all) > 1 ) {
        my ($type) = @_;

        # Determine the distribution of bits at cur_i
        for ( my $i = 0 ; $i < scalar(@all) ; $i++ ) {
            my @cur = grep /\d+/, split //, $all[$i];
            my $cur = $cur[$cur_i];

            $dist++ if $cur;
            $dist-- if not $cur;
        }

        # Determine items that do not conform from array
        for ( my $i = 0 ; $i < scalar(@all) ; $i++ ) {
            my @cur    = grep /\d+/, split //, $all[$i];
            my $cur    = $cur[$cur_i];
            my $common = ( $dist >= 0 )         ? 1       : 0;
            my $rare   = ( $dist >= 0 )         ? 0       : 1;
            my $y      = ( $type =~ /oxygen/i ) ? $common : $rare;
            my $noteq  = $cur != $y;

            if ($noteq) {
                push @del, ($i);
            }
        }

        # Delete items
        for ( sort { $b <=> $a } @del ) {
            splice @all, $_, 1;
        }

        # Go to next cur_i and reset
        @del  = ();
        $dist = 0;
        $cur_i++;

    }

    return oct( "0b" . $all[0] );
}

my $oxygen = findRating "oxygen";
my $co2    = findRating "co2";

say $oxygen * $co2;

close $fh;
