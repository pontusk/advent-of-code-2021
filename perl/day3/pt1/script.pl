#!/usr/bin/env perl
use v5.14;

open my $fh, "<", "../input" or die "Couldn't open 'input': $!";

my @nums = ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 );

for (<$fh>) {
    my @cur   = grep /\d+/, split //, $_;
    my $index = 0;

    for (@cur) {

        $nums[$index]++ if $_;
        $nums[$index]-- if not $_;

        $index++;
    }

    $index = 0;
}

my $gamma   = oct( "0b" . join "", map { $_ > 0 ? 1 : 0 } @nums );
my $epsilon = oct( "0b" . join "", map { $_ < 0 ? 1 : 0 } @nums );

say $gamma * $epsilon;

close $fh;
