import io
import collections

with io.open("../input", "r", encoding="utf8") as f:
    data = f.read()

connections = collections.defaultdict(set)
for line in data.splitlines():
    first, second = line.split("-")
    connections[first].add(second)
    connections[second].add(first)

all_paths = set()
todo = []
todo.append(("start",))

while todo:
    path = todo.pop()

    if path[-1] == "end":
        all_paths.add(path)
        continue

    for connection in connections[path[-1]]:
        if connection.isupper() or connection not in path:
            todo.append((*path, connection))

print(len(all_paths))
