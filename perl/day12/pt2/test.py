import io
import collections

with io.open("../test_input", "r", encoding="utf8") as f:
    data = f.read()

connections = collections.defaultdict(set)
for line in data.splitlines():
    first, second = line.split("-")
    connections[first].add(second)
    connections[second].add(first)

all_paths = set()
todo = []
todo.append((("start",), False))

while todo:
    path, double_cave = todo.pop()

    if path[-1] == "end":
        all_paths.add(path)
        continue

    for connection in connections[path[-1]]:
        if connection == "start":
            continue
        elif connection.isupper() or connection not in path:
            todo.append(((*path, connection), double_cave))
        elif not double_cave and path.count(connection) == 1:
            todo.append(((*path, connection), True))


print(len(all_paths))
