#!/usr/bin/env perl
use v5.14;
use Data::Dumper;
use POSIX;

open my $fh, "<", "../input" or die "Couldn't open 'input': $!";

my @rest;
my %pairs = (
    "(" => ")",
    "{" => "}",
    "[" => "]",
    "<" => ">"
);
my %points = (
    ")" => 1,
    "]" => 2,
    "}" => 3,
    ">" => 4
);

for (<$fh>) {
    my $rest  = $_;
    my $regex = qr/\(\)|\{\}|\[\]|\<\>/;

    my $extract_match = sub {
        $rest = $rest =~ s/$regex//r;
    };

    $extract_match->() while $rest =~ s/$regex//;

    my ($corrupted_pair) = $rest =~ /([\(\{\[\<][\)\}\]\>])/;

    if ( not $corrupted_pair ) {
        my @arr = reverse split //, $rest;
        push @rest, ( \@arr );
    }
}

my @scores;

for (@rest) {
    my @completion = grep /\S+/, map { $pairs{$_} } @{$_};
    my $score      = 0;

    for (@completion) {
        $score = $score * 5 + $points{$_};
    }

    push @scores, ($score);
}

my @sorted = sort { $a <=> $b } @scores;
say $sorted[ POSIX::ceil( $#scores / 2 ) ];

close $fh;
