#!/usr/bin/env perl
use v5.14;
use Data::Dumper;

open my $fh, "<", "../input" or die "Couldn't open 'input': $!";

my @corrupted;
my %points = (
    ")" => 3,
    "]" => 57,
    "}" => 1197,
    ">" => 25137
);

for (<$fh>) {
    my $rest  = $_;
    my $regex = qr/\(\)|\{\}|\[\]|\<\>/;

    my $extract_match = sub {
        $rest = $rest =~ s/$regex//r;
    };

    $extract_match->() while $rest =~ s/$regex//;

    my ($corrupted_pair) = $rest =~ /([\(\{\[\<][\)\}\]\>])/;
    push @corrupted, ( substr $corrupted_pair, 1, 2 ) if $corrupted_pair;
}

my $sum;

for (@corrupted) {
    $sum += $points{$_};
}

say $sum;

close $fh;
