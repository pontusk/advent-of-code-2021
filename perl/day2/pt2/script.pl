#!/usr/bin/env perl
use v5.14;

open my $fh, "<", "../input" or die "Couldn't open 'input': $!";

my $hor = 0;
my $aim = 0;
my $dep = 0;

for (<$fh>) {
    my ( $ins, $val ) = split / /, $_;

    $aim += $val        if $ins =~ /down/;
    $aim -= $val        if $ins =~ /up/;
    $hor += $val        if $ins =~ /forward/;
    $dep += $val * $aim if $ins =~ /forward/;

}

say $hor * $dep;

close $fh;
