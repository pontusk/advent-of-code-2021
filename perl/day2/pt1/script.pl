#!/usr/bin/env perl
use v5.14;

open my $fh, "<", "../input" or die "Couldn't open 'input': $!";

my $hor = 0;
my $dep = 0;

for (<$fh>) {
    my ( $ins, $val ) = split / /, $_;

    $hor += $val if $ins =~ /forward/;
    $dep -= $val if $ins =~ /up/;
    $dep += $val if $ins =~ /down/;

}

say $hor * $dep;

close $fh;
