#!/usr/bin/env perl
use v5.14;
use Data::Dumper;

open my $fh, "<", "../test_input" or die "Couldn't open 'input': $!";

my @fish = map { s/\s//r } split /,/, <$fh>;
my $i    = 0;

for ( 0 .. 79 ) {
    $i = 0;

    for (@fish) {
        if ( $fish[$i] == 0 ) {
            $fish[$i] = 6;
            push @fish, (9);
        }
        else {
            $fish[$i]--;
        }

        $i++;
    }
}

say scalar @fish;

close $fh;
