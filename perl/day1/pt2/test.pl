#!/usr/bin/env perl
use v5.14;
use Data::Dumper;

open my $fh, "<", "../test_input" or die "Couldn't open 'input': $!";

my @nums = <$fh>;
my $inc  = 0;
my $sum  = 0;

for ( @nums[ 0 .. 2 ] ) {
    $sum += $_;
}

for (<@nums>) {
    state $index;
    state $prev;

    $inc++ if $prev and $sum > $prev;

    $prev = $sum;
    $sum  = $sum - $_ + $nums[ $index + 3 ];

    $index++;
}

say $inc;

close $fh;
