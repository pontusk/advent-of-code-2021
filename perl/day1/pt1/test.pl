#!/usr/bin/env perl
use v5.14;

open my $fh, "<", "../test_input" or die "Couldn't open 'input': $!";

my $count = 0;

for (<$fh>) {
    state $prev = 0;
    $count++ if $_ > $prev and $prev != 0;
    $prev = $_;
}

say $count;

close $fh;
