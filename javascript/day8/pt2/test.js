const fs = require("fs");
const data = fs
  .readFileSync("../test_input", { encoding: "utf8" })
  .split("\n")
  .filter((it) => it !== "")
  .map((it) =>
    it.split("|").map((it) => it.split(" ").filter((it) => it !== ""))
  );

const digitRef = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
const patternRef = [
  "abcefg",
  "cf",
  "acdeg",
  "acdfg",
  "bcdf",
  "abdfg",
  "abdefg",
  "acf",
  "abcdefg",
  "abcdfg",
];

class Digit {
  id = null;
  patternRef = "";
  possiblePatterns = [];
  solved = false;
  constructor(id, patterns) {
    this.id = id;
    this.patternRef = patternRef[id];

    this.possiblePatterns = patterns.filter(
      (pattern) => pattern.length === this.patternRef.length
    );

    this.checkIfSolved();
  }

  checkPattern(digit, getSolved) {
    if (this.solved) return;
    const solved = getSolved();
    const possiblePatterns = this.possiblePatterns.filter((pattern) => {
      if (solved.some((it) => it.possiblePatterns[0] == pattern)) {
        return false;
      }
      return digit.possiblePatterns[0]
        .split("")
        .every((it) => pattern.split("").includes(it));
    });

    if (!possiblePatterns.length) return;
    this.possiblePatterns = possiblePatterns;
    this.checkIfSolved();
  }

  checkPatternReverse(digit, getSolved) {
    if (this.solved) return;
    const solved = getSolved();
    const possiblePatterns = this.possiblePatterns.filter((pattern) => {
      if (solved.some((it) => it.possiblePatterns[0] == pattern)) {
        return false;
      }
      return pattern
        .split("")
        .every((it) => digit.possiblePatterns[0]?.split("").includes(it));
    });

    if (!possiblePatterns.length) return;
    this.possiblePatterns = possiblePatterns;
    this.checkIfSolved();
  }

  checkIfSolved() {
    if (this.possiblePatterns.length === 1) this.solved = true;
  }
}

class Display {
  digits = [];
  patterns = [];
  output = [];
  decodedOutput = 0;
  constructor(patterns, output) {
    const _patterns = patterns.map((it) => it.split("").sort().join(""));
    this.digits = digitRef.map((digit) => new Digit(digit, _patterns));
    this.patterns = _patterns;
    this.output = output.map((it) => it.split("").sort().join(""));
  }

  checkPatterns() {
    this.digits[9].checkPattern(this.digits[4], this.getSolved);
    this.digits[0].checkPattern(this.digits[7], this.getSolved);
    this.digits[3].checkPattern(this.digits[1], this.getSolved);
    this.digits[6].checkPatternReverse(this.digits[8], this.getSolved);
    this.digits[5].checkPatternReverse(this.digits[6], this.getSolved);
    this.digits[2].checkPatternReverse(this.digits[8], this.getSolved);
  }

  decodeOutput() {
    const decodedOutput = this.output
      .map((it) => {
        const digit = this.digits.find((d) => d.possiblePatterns[0] === it);
        if (digit !== undefined) {
          return digit.id;
        }
      })
      .filter((it) => it !== undefined)
      .join("");
    this.decodedOutput = parseInt(decodedOutput);
  }

  getSolved = () => {
    return this.digits.filter((it) => it.solved);
  };
}

const displays = data.map((line) => {
  const [patterns, output] = line;
  return new Display(patterns, output);
});

displays.forEach((display) => display.checkPatterns());
//
// displays[0].checkPatterns();
// displays[0].decodeOutput();
// displays[1].checkPatterns();
// displays[1].decodeOutput();

const result = displays.reduce((acc, cur) => {
  cur.decodeOutput();
  acc += cur.decodedOutput;
  return acc;
}, 0);

console.log(result);

// const testDisplay = displays[1];
//
// console.log(JSON.stringify(testDisplay, undefined, 2));

// console.log(
//   "Solved digits: ",
//   testDisplay.digits.filter((it) => it.solved).map((it) => it.id)
// );
