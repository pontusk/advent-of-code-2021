const fs = require("fs");

class Point {
  height = 0;
  id = "";
  c = 0;
  l = 0;
  constructor(height) {
    this.height = parseInt(height);
  }
  setProperties(id, l, c) {
    this.id = id;
    this.c = c;
    this.l = l;
  }
}

const data = fs
  .readFileSync("../test_input", { encoding: "utf8" })
  .split("\n")
  .map((it) => it.split("").map((it) => new Point(it)))
  .filter((it) => it.length);

const basins = [];

class Basin {
  start = {};
  surrounding = [];
  basinSize = [];
  isLowPoint = false;

  constructor(point) {
    this.start = point;
    this.surrounding = [point];
    this.setSurroundingPoints(point);
  }

  explore(point) {
    const sur = this.findSurrounding(point);
    sur.forEach((s) => {
      if (this.basinSize.some((p) => p.id === s.id)) return;
      if (s.height === 9) return;
      this.basinSize.push(s);
      this.explore(s);
    });
  }

  setSurroundingPoints(point) {
    const sur = this.findSurrounding(point);
    sur.forEach((s) => {
      this.surrounding.push(s);
    });

    let lowest = sur[0];
    sur.forEach((p) => {
      if (p.height < lowest.height) lowest = p;
    });

    if (point.height < lowest.height) {
      this.isLowPoint = true;
      this.basinSize = sur.filter((it) => it.height !== 9) || [];
      this.basinSize.push(point);

      this.basinSize.forEach((s) => {
        this.explore(s);
      });
    }
  }

  findSurrounding(point) {
    const { l, c } = point;
    return [
      //top
      data?.[l - 1]?.[c],
      //right
      data?.[l]?.[c + 1],
      //bottom
      data?.[l + 1]?.[c],
      //right
      data?.[l]?.[c - 1],
    ].filter((it) => it);
  }
}

function loop(fn) {
  for (let l = 0, len = data.length; l < len; l++) {
    const line = data[l];
    for (let c = 0, len = line.length; c < len; c++) {
      fn(l, c);
    }
  }
}

loop((l, c) => data[l][c].setProperties(`${l}-${c}`, l, c));
loop((l, c) => basins.push(new Basin(data[l][c])));

console.log(
  basins
    .filter((it) => it.isLowPoint)
    .sort((a, b) => {
      return b.basinSize.length - a.basinSize.length;
    })
    .map((it) => it.basinSize.length)
    .splice(0, 3)
    .reduce((acc, cur) => {
      acc = acc ? acc * cur : cur;
      return acc;
    }, 0)
);
