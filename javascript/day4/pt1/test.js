const fs = require('fs');

const data = fs
  .readFileSync('../test_input', { encoding: 'utf8' })
  .replace(/[, ]+/g, ' ')
  .split('\n')
  .filter((it) => it !== '')
  .map((it) => it.trim());

const [drawn, ...rest] = data;

const drawnNumbers = drawn.split(' ');

let boards = rest.reduce((acc, cur, i) => {
  const index = Math.floor(i / 5);
  const prev = acc[index] || [];
  acc[index] = [...prev, cur.split(' ')];
  return acc;
}, []);

class Board {
  numbers = [];
  index = 0;
  marked = [];
  rows = [];
  columns = [];
  winner = false;

  constructor(boardNumbers, index) {
    this.numbers = boardNumbers;
    this.index = index;
  }

  findNumber(number) {
    let col = -1;
    let row = -1;
    this.numbers.forEach((r, i) => {
      const c = r.findIndex((n) => n === number);
      col = c > col ? c : col;
      row = c > row ? i : row;
    });

    return { col, row };
  }

  markNumber(number) {
    this.marked.push(number);
    const { col, row } = this.findNumber(number);
    const prevRows = this.rows[row] || [];
    const prevCols = this.columns[col] || [];
    if (row >= 0) this.rows[row] = [...prevRows, number];
    if (col >= 0) this.columns[col] = [...prevCols, number];

    if (this.rows[row]?.length >= 5 || this.columns[col]?.length >= 5) {
      this.winner = true;
    }
  }

  getScore() {
    const score = this.numbers
      .reduce((acc, cur) => {
        acc = acc.concat(cur);
        return acc;
      }, [])
      .filter((n) => !this.marked.some((m) => n === m))
      .reduce((a, b) => parseInt(a) + parseInt(b), 0);
    return score;
  }
}

boards = boards.map((n, i) => new Board(n, i));

for (let i = 0, len = drawnNumbers.length; i < len; i++) {
  let endGame = false;
  const number = drawnNumbers[i];
  for (let i = 0, len = boards.length; i < len; i++) {
    const board = boards[i];
    board.markNumber(number);
    if (board.winner) {
      endGame = true;
      break;
    }
  }
  if (endGame) break;
}

const winner = boards.find((b) => b.winner);

const score = winner.getScore();
const lastDrawn = winner.marked.pop();

console.log(score * lastDrawn);
