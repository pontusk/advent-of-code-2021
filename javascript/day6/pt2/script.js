const fs = require('fs');

const data = fs
  .readFileSync('../input', { encoding: 'utf8' })
  .split(',')
  .map((it) => parseInt(it))
  .reduce((acc, cur) => {
    const prev = acc[cur] || 0;
    acc[cur] = prev + 1;
    return acc;
  }, {});

const defaultAges = {
  0: 0,
  1: 0,
  2: 0,
  3: 0,
  4: 0,
  5: 0,
  6: 0,
  7: 0,
  8: 0,
};

let daysPassed = 0;
let ages = { ...defaultAges, ...data };

while (daysPassed < 256) {
  const newAges = { ...defaultAges };

  Object.keys(newAges)
    .sort((a, b) => b - a)
    .forEach((key) => {
      if (key === '0') {
        newAges[8] = ages[0];
        newAges[6] += ages[0];
      } else {
        newAges[key - 1] = ages[key];
      }
    });

  ages = newAges;

  daysPassed++;
}

const result = Object.values(ages).reduce((a, b) => a + b);

console.log('result: ', result);
