const fs = require("fs");

class Octopus {
  energy = 0;
  l = 0;
  c = 0;
  id = "";
  flash = "init";

  constructor(energy, l, c) {
    this.energy = energy;
    this.l = l;
    this.c = c;
    this.id = `${l}-${c}`;
  }

  increaseEnergy() {
    this.energy++;

    if (this.energy > 9 && this.flash === "init") {
      this.flash = "active";
    }
  }

  reset() {
    if (this.flash !== "init") {
      this.energy = 0;
    }
    this.flash = "init";
  }

  findSurrounding() {
    const { l, c } = this;
    return [
      //top
      data?.[`${l - 1}-${c}`]?.id,
      // top right
      data?.[`${l - 1}-${c + 1}`]?.id,
      //right
      data?.[`${l}-${c + 1}`]?.id,
      //bottom right
      data?.[`${l + 1}-${c + 1}`]?.id,
      //bottom
      data?.[`${l + 1}-${c}`]?.id,
      //bottom left
      data?.[`${l + 1}-${c - 1}`]?.id,
      //left
      data?.[`${l}-${c - 1}`]?.id,
      //top left
      data?.[`${l - 1}-${c - 1}`]?.id,
    ].filter((it) => it);
  }
}

const data = fs
  .readFileSync("../input", { encoding: "utf8" })
  .split("\n")
  .map((it, l) => it.split("").map((it, c) => new Octopus(parseInt(it), l, c)))
  .filter((it) => it.length)
  .reduce((outerAcc, outerCur) => {
    outerAcc = {
      ...outerAcc,
      ...outerCur.reduce((acc, cur) => {
        acc[cur.id] = cur;
        return acc;
      }, {}),
    };
    return outerAcc;
  }, {});

function initiateIncrease(ids) {
  ids.forEach((id) => {
    data[id].increaseEnergy();
  });
  ids.forEach((id) => {
    if (data[id].flash === "active" && data[id].energy < 20) {
      data[id].flash = "afterburn";
      initiateIncrease(data[id].findSurrounding());
    }
  });
}

function resetOctopi(ids) {
  ids.forEach((id) => data[id].reset());
}

let steps = 0;

while (!Object.values(data).every((it) => it.energy === 0)) {
  initiateIncrease(Object.keys(data));
  resetOctopi(Object.keys(data));
  steps++;
}

console.log(steps);
