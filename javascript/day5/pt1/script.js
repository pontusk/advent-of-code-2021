const fs = require('fs');

const data = fs
  .readFileSync('../input', { encoding: 'utf8' })
  .split('\n')
  .map((it) => it.split(' -> '))
  .filter((it) => !it.includes(''));

const vents = {};

for (let i = 0, len = data.length; i < len; i++) {
  const [start, end] = data[i];

  const [x1, y1] = start.split(',').map((it) => parseInt(it));
  const [x2, y2] = end.split(',').map((it) => parseInt(it));

  const xDif = Math.abs(x1 - x2);
  const yDif = Math.abs(y1 - y2);

  const xDir = x1 - x2 > 0 ? 'minus' : x1 === x2 ? 'equal' : 'plus';
  const yDir = y1 - y2 > 0 ? 'minus' : y1 === y2 ? 'equal' : 'plus';

  const difMax = Math.max(xDif, yDif);
  const difMin = Math.min(xDif, yDif);
  const smallest = xDif - yDif > 0 ? 'y' : 'x';
  const factor = difMin / difMax;

  if (difMin > 0) continue;

  for (let index = 0, len = difMax + 1; index < len; index++) {
    let x;
    if (xDir === 'plus') {
      x = Math.floor(x1 + (smallest === 'x' ? factor : index));
    } else if (xDir === 'minus') {
      x = Math.floor(x1 - (smallest === 'x' ? factor : index));
    } else {
      x = x1;
    }

    let y;
    if (yDir === 'plus') {
      y = Math.floor(y1 + (smallest === 'y' ? factor : index));
    } else if (yDir === 'minus') {
      y = Math.floor(y1 - (smallest === 'y' ? factor : index));
    } else {
      y = y1;
    }

    const key = `x${x}y${y}`;

    const prev = vents[key] || 0;

    vents[key] = prev + 1;
  }
}

const ventCount = Object.values(vents).reduce((acc, cur) => {
  if (cur > 1) acc++;
  return acc;
}, 0);

console.log(ventCount);
