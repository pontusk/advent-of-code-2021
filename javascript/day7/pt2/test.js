const fs = require('fs');

const data = fs
  .readFileSync('../test_input', { encoding: 'utf8' })
  .split(',')
  .map((it) => parseInt(it));

let range = data.reduce(
  (acc, cur) => {
    if (cur < acc[0]) {
      acc[0] = cur;
    }
    if (cur > acc[1]) {
      acc[1] = cur;
    }
    return acc;
  },
  [0, data[1]]
);

let guessedPos = range[0];
let fuelCost = 0;

for (let i = 0, len = range[1] - range[0]; i < len; i++) {
  const newFuelCost = data.reduce((acc, cur) => {
    const n = Math.abs(guessedPos - cur);
    // Arithmetic series
    acc += (n * (n + 1)) / 2;
    return acc;
  }, 0);

  if (fuelCost === 0 || newFuelCost < fuelCost) fuelCost = newFuelCost;

  guessedPos = range[0] + i;
}

console.log(fuelCost);
